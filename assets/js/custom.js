function init() {
    tooltips(), onePageScroll(), scrollAnchor(), toggleContactForm()
}

function owlCarousel() {
    $("#owl-example").owlCarousel({
        lazyLoad: !0,
        items: 3,
        theme: "owl-theme-main"
    }), $("#intro").owlCarousel({
        lazyLoad: !0,
        lazyEffect: "fade",
        singleItem: !0,
        navigation: !0,
        navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        slideSpeed: 450,
        pagination: !1,
        transitionStyle: "fade",
        theme: "owl-theme-featured"
    })
}

function tooltips() {
    $(".tooltips").tooltip()
}

function magnificPopup() {
    $(".popup-gallery").magnificPopup({
        type: "image",
        tLoading: "Loading image #%curr%...",
        mainClass: "mfp-fade",
        disableOn: 700,
        removalDelay: 160,
        gallery: {
            enabled: !0,
            navigateByImgClick: !0,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        },
        callbacks: {
            close: function() {
                $(".portfolio-item figure figcaption").removeClass("active"), $(".portfolio-item figure .info").removeClass("active")
            }
        }
    }), $(".portfolio-item figcaption a.preview").click(function() {
        $(this).parent().addClass("active"), $(this).parent().siblings(".info").addClass("active")
    }), $(".zoom-modal").magnificPopup({
        type: "image",
        mainClass: "mfp-with-zoom",
        zoom: {
            enabled: !0,
            duration: 300,
            easing: "ease-in-out",
            opener: function(e) {
                return e.is("i") ? e : e.find("i")
            }
        }
    }), $(".popup-modal").magnificPopup({
        type: "inline",
        fixedContentPos: !1,
        fixedBgPos: !0,
        overflowY: "auto",
        closeBtnInside: !0,
        preloader: !1,
        midClick: !0,
        removalDelay: 300,
        mainClass: "my-mfp-slide-bottom"
    })
}

function isotope() {
    var e = $("#portfolio");
    e.imagesLoaded(function() {
        e.isotope({
            itemSelector: ".portfolio-item",
            layoutMode: "fitRows"
        })
    }), $("#filters").on("click", "button", function(t) {
        var a = $(this).attr("data-filter-value");
        e.isotope({
            filter: a
        }), $("#filters button").removeClass("active"), $(this).addClass("active")
    })
}

function scrollAnchor() {
    $(".scroll").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"), e.length) return $("html,body").animate({
                scrollTop: e.offset().top - 72
            }, 650), !1
        }
    })
}

function onePageScroll() {
    $(".nav").onePageNav({
        currentClass: "current",
        changeHash: !1,
        scrollSpeed: 650,
        scrollOffset: 30,
        scrollThreshold: .5,
        filter: ":not(.login, .signup, .external)",
        easing: "swing",
        begin: function() {},
        end: function() {},
        scrollChange: function(e) {}
    })
}
$(window).load(function() {
    $(".preloader").fadeOut(1e3), init()
}), $(document).ready(function() {
    owlCarousel(), magnificPopup()
}), window.scrollReveal = new scrollReveal, $(window).scroll(function() {
    var e = $(window).scrollTop();
    500 >= e && $(".nav li.current").removeClass("current")
}), $(function() {
    var e = document.createElement("input");
    "placeholder" in e == 0 && $("[placeholder]").focus(function() {
        var e = $(this);
        e.val() == e.attr("placeholder") && (e.val("").removeClass("placeholder"), e.hasClass("password") && (e.removeClass("password"), this.type = "password"))
    }).blur(function() {
        var e = $(this);
        "" != e.val() && e.val() != e.attr("placeholder") || ("password" == this.type && (e.addClass("password"), this.type = "text"), e.addClass("placeholder").val(e.attr("placeholder")))
    }).blur().parents("form").submit(function() {
        $(this).find("[placeholder]").each(function() {
            var e = $(this);
            e.val() == e.attr("placeholder") && e.val("")
        })
    })
}), $("#name").focus(function() {
    $("#success").html("")
});

// Animations
//-----------------------------------------------
if ($("[data-animation-effect]").length>0) {
    $("[data-animation-effect]").each(function() {
        if(Modernizr.csstransitions) {
            var waypoints = $(this).waypoint(function(direction) {
                var appearDelay = $(this.element).attr("data-effect-delay"),
                animatedObject = $(this.element);
                setTimeout(function() {
                    animatedObject.addClass('animated object-visible ' + animatedObject.attr("data-animation-effect"));
                }, appearDelay);
                this.destroy();
            },{
                offset: '90%'
            });
        } else {
            $(this).addClass('object-visible');
        }
    });
};

// Isotope filters
//-----------------------------------------------
if ($('.isotope-container').length>0 || $('.masonry-grid').length>0 || $('.masonry-grid-fitrows').length>0 || $('.isotope-container-fitrows').length>0) {
    $(window).load(function() {
        $('.masonry-grid').isotope({
            itemSelector: '.masonry-grid-item',
            layoutMode: 'masonry'
        });
        $('.masonry-grid-fitrows').isotope({
            itemSelector: '.masonry-grid-item',
            layoutMode: 'fitRows'
        });
        $('.isotope-container').fadeIn();
        var $container = $('.isotope-container').isotope({
            itemSelector: '.isotope-item',
            layoutMode: 'masonry',
            transitionDuration: '0.6s',
            filter: "*"
        });
        $('.isotope-container-fitrows').fadeIn();
        var $container_fitrows = $('.isotope-container-fitrows').isotope({
            itemSelector: '.isotope-item',
            layoutMode: 'fitRows',
            transitionDuration: '0.6s',
            filter: "*"
        });
        // filter items on button click
        $('.filters').on( 'click', 'ul.nav li a', function() {
            var filterValue = $(this).attr('data-filter');
            $(".filters").find("li.active").removeClass("active");
            $(this).parent().addClass("active");
            $container.isotope({ filter: filterValue });
            $container_fitrows.isotope({ filter: filterValue });
            return false;
        });
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('.tab-pane .masonry-grid-fitrows').isotope({
            itemSelector: '.masonry-grid-item',
            layoutMode: 'fitRows'
        });
    });
};
