    <!-- NAVBAR
    ================================================== -->
    <div class="navbar-wrapper cbp-af-header cbp-af-header-shrink">
      <div class="container">

        <!-- Fixed navbar -->
        <div class="navbar cbp-af-inner" role="navigation">
          <div class="row">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <!-- LOGO VECTOR FORMAT
              ================================================== -->
              <a class="logo" href="/"><i class="fa icon-logo-cp"></i></a>

            </div>

            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav pull-right">
                <li><a href="/" class="external">Home</a></li>
                <li class="hidden-sm"><a href="/about/" class="external">About</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Legal Solutions <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/legal-solutions/civil-litigation/" class="external">Civil Litigation</a></li>
                    <li><a href="/legal-solutions/corporate-and-commercial/" class="external">Corporate &amp; Commercial</a></li>
                    <li><a href="/legal-solutions/criminal-defence/" class="external">Criminal Defence</a></li>
                    <li><a href="/legal-solutions/family-law/" class="external">Family Law</a></li>
                    <li><a href="/legal-solutions/immigration/" class="external">Immigration</a></li>
                    <li><a href="/legal-solutions/labour-law/" class="external">Labour Law</a></li>
                    <li><a href="/legal-solutions/real-estate/" class="external">Real Estate</a></li>
                    <li><a href="/legal-solutions/wills-and-estates/" class="external">Wills &amp; Estates</a></li>
                  </ul>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Our Lawyers <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/our-lawyers/bob-mayer/" class="external">Bob Mayer</a></li>
                    <li><a href="/our-lawyers/ron-dearman/" class="external">Ron Dearman</a></li>
                    <li><a href="/our-lawyers/rob-pellizzaro/" class="external">Rob Pellizzaro</a></li>
                    <!-- REMOVE THIS TO ACTIVATE TYRONE
                    <li><a href="/our-lawyers/tyrone-krawetz/" class="external">Tyrone Krawetz</a></li>
                    REMOVE THIS TO ACTIVATE TYRONE -->
                  </ul>
                </li>
                <li class="dropdown hidden-sm">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Resources <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="/blog/" class="external">Blog</a></li>
                    <li><a href="/faq/" class="external">FAQ</a></li>
                    <li><a href="/resources/" class="external">Legal Resources</a></li>
                  </ul>
                </li>
                <li><a href="/contact/" class="external">Contact</a></li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>