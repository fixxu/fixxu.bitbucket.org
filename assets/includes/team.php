    <!-- Team
    ================================================== -->

    <section id="team" class="light-gray-bg">
      <div class="container">
        <div class="row">
          <div class="page-header text-center">
            <h2>Our Lawyers</h2>
          </div>
          <div class="col-md-12">
            <div id="owl-example" class="owl-carousel">
              <div class="item">
                <img class="display-pic" src="/assets/images/bob-mayer.jpg" alt="Team Member">
                  <h3>Bob Mayer</h3>
                  <h4>Founding Partner</h4>
                  <h4>(1970-2016, deceased)</h4>
                  <p>Bob Mayer was one of the founding partners of the firm and had a distinguished 45 year career. He gained a well deserved reputation for serving his clients with integrity and skill. Bob retired from practice on February 29, 2016 and has since passed away. His legacy continues in the law firm that he started.</p>
                  <p><a class="btn btn-lg btn-primary scroll" href="/our-lawyers/bob-mayer/" role="button">View Profile</a></p>
              </div>
              <div class="item">
                <img class="display-pic" src="/assets/images/ron-dearman.jpg" alt="Team Member">
                  <h3>Ron Dearman</h3>
                  <h4>Partner</h4>
                  <p>Practising in the areas of real estate, corporate and commercial law, wills &amp; estates, Ron's depth of knowledge and experience in these areas guarantees you are in safe hands whether you're in the process of buying and selling a home, dealing with the sale or purchase of a business or wanting to get your estate in order.</p>
                  <p><a class="btn btn-lg btn-primary scroll" href="/our-lawyers/ron-dearman/" role="button">View Profile</a></p>
              </div>
              <div class="item">
                <img class="display-pic" src="/assets/images/rob-pellizzaro.jpg" alt="Team Member">
                  <h3>Rob Pellizzaro</h3>
                  <h4>Partner</h4>
                  <p>Robert Pellizzaro practices in the areas of Wills and Estates and Family law. He has extensive experience with issues of separation and divorce, custody and access, child and spousal support, property division, adoption and guardianship, including advice, negotiation, litigation, and preparation of pleadings and domestic agreements.</p>
                  <p><a class="btn btn-lg btn-primary scroll" href="/our-lawyers/rob-pellizzaro/" role="button">View Profile</a></p>
              </div>
              <!-- REMOVE THIS TO ACTIVATE TYRONE
              <div class="item">
                <img class="display-pic" src="/assets/images/tyrone-krawetz.jpg" alt="Team Member">
                  <h3>Tyrone L. Krawetz</h3>
                  <h4>Lawyer</h4>
                  <p>Tyrone enjoys helping people navigate the complexities of the law. He has a general practice with an emphasis on criminal defence. He earned a Political Science degree from the University of Winnipeg for which he was awarded the Gold Medal, and a Law degree from the University of Manitoba.</p>
                  <p><a class="btn btn-lg btn-primary scroll" href="/our-lawyers/rob-pellizzaro/" role="button">View Profile</a></p>
              </div>
              REMOVE THIS TO ACTIVATE TYRONE -->
            </div>
          </div>
        </div>
      </div>
    </section>